<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/CustomerRelationshipManagementProject.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link href="/System/_css/New.css" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="/System/_scripts/Default.js" type="text/JavaScript"></script>
<script language="JavaScript" src="/System/_scripts/MainNavigation.js" type="text/JavaScript"></script>
<!-- InstanceBeginEditable name="TITLE" --><title>Tutorials - Customer Relationship Management Project - InsideHRM</title><!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="META" --><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /><!-- InstanceEndEditable -->

<!-- InstanceBeginEditable name="HEAD" --><!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="INCLUDES" --><!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="SCRIPTING" -->

<script type="text/javascript">
//<![CDATA[

//field character countdown
function textCounter(field, countfield, maxlimit) {
	if (field.value.length > maxlimit) // if too long...trim it!
	field.value = field.value.substring(0, maxlimit);
	// otherwise, update 'characters left' counter
	else 
	countfield.value = maxlimit - field.value.length;
}

//]]>
</script>

<style type="text/css">
<!--
input.main {	width:300px;
}
select.main {	width:305px;
}
textarea.description {	width:99%;
}
-->
</style>
<!-- InstanceEndEditable -->

   
   
   
   
   <!-- InstanceParam name="Sidelinks" type="boolean" value="true" -->
   
   
      <style type="text/css">
	  <!--
         #ContentArea {
            margin-right:215px;
         }
	  -->
      </style>
   
   	  <script language="javascript" type="text/javascript">
	  var output = "<link href='/System/_css/DynamicStyleSizes.css' rel='stylesheet' type='text/css' media='screen' />";
	  document.write(output);
	  </script>

</head>

<body onload="commonScripts();">

<div id="OuterPerimeter">
	<div id="LeftBorder">
		<div id="RightBorder">
			<div id="InnerContent">

			
				<!--#include virtual="/System/_includes/SiteHeader.html"-->
				<!--#include virtual="/System/_includes/CriticalAlert.html"-->
				<div id="BreadCrumbs">
				<!--#include virtual="/System/_includes/BreadCrumbs.html"-->
				</div>
				
				<div id="SectionBanner">
					<div id="SectionBannerGraphic"><!--#include virtual="/CorporateProjects/CustomerRelationshipManagementProject/_banner.html"--></div>
					<div id="SectionBannerTitle"><a href="/CorporateProjects/CustomerRelationshipManagementProject/">Customer Relationship Management Project</a></div>
					<div class="ClearBoth"></div>
			  	</div>
				<div id="SectionNav">
					
					<div id="SectionNavIcons">
						<span id="SectionAboutUs" class="SectionAboutUs"><a href="/CorporateProjects/CustomerRelationshipManagementProject/AboutUs.html" rel="nofollow" onmouseover="JSFX.fadeIn('AboutUs')" onmouseout="JSFX.fadeOut('AboutUs')"><img id="AboutUs" name="AboutUs" class="imgFader" src="/System/_images/AboutUsIcon.jpg" alt="About Us" title="About Us" border="0" /></a></span>
						<span id="SectionContactUs"  class="SectionContactUs"><a href="/CorporateProjects/CustomerRelationshipManagementProject/ContactUs.html" rel="nofollow" onmouseover="JSFX.fadeIn('ContactUs')" onmouseout="JSFX.fadeOut('ContactUs')"><img id="ContactUs" name="ContactUs" class="imgFader" src="/System/_images/ContactUsIcon.jpg" alt="Contact Us" title="Contact Us" border="0" /></a></span><span id="SectionHome" class="SectionHome"><a href="/CorporateProjects/CustomerRelationshipManagementProject/" rel="nofollow" onmouseover="JSFX.fadeIn('SectionHome')" onmouseout="JSFX.fadeOut('SectionHome')"><img id="SectionHome" name="SectionHome" class="imgFader" src="/System/_images/SectionHomeIcon.jpg" alt="Customer Relationship Management Project Homepage" title="Customer Relationship Management Project Homepage" border="0" /></a></span></div>
					
					<div id="SectionNavLinks"><!--#include virtual="/CorporateProjects/CustomerRelationshipManagementProject/_menu.html"--></div>
					<div class="ClearBoth"></div>
				</div>
				<div id="MainContentArea">
					
					<div id="Sidelinks">
						<div id="SidelinksHeaderArea">
							<div id="SidelinkArrows">
								<span id="SidelinkLeft" class="SidelinkLeft"><a href="javascript:move_sidelinks('left');"><img src="/System/_images/ArrowGoLeft.gif" alt="Click to move the menu to the left side" title="Click to move the menu to the left side" border="0" /></a></span>
								<span id="SidelinkRight" class="SidelinkRight"><a href="javascript:move_sidelinks('right');"><img src="/System/_images/ArrowGoRight.gif" alt="Click to move the menu to the right side" title="Click to move the menu to the right side" border="0" /></a></span>							</div>
							<div id="SidelinkHeader">Additional Information</div>
						</div>
						<div id="SidelinksContent"><!-- InstanceBeginEditable name="SIDELINK_REGION" --><p>Would you like more information? </p>
						  <p>View our <a href="Documents/QuickReferenceGuide.pdf">Quick Reference Guide</a>. Or check out the various <a href="../../BusinessUnits/BusinessSolutions/UserGuides.html">User Guides</a> on the Business Solutions Page. </p>
						<!-- InstanceEndEditable -->						</div>
					</div>
					
					<div id="ContentArea">
					<!-- InstanceBeginEditable name="CONTENT_REGION" -->
					<form action="http://eservices.halifax.ca/_private/phpsendmail/formprocess.php" method="post" name="CRM Tutorials" id="CRM Tutorial">
                      <p>
								<input type="hidden" name="form_name" value="CRM Tutorials" />

								<span class="Header2">Thank you for taking the time to watch the new CRM tutorial(s).                                </span>
                      <p>&nbsp;</p>
                      <p>The CRM Tutorial Prize draw date was Tuesday, February 27th. Join us in congratulating the following staff who have won the Tutorial Prizes:</p>
                      <table width="577" border="1">
                        <tr valign="top">
                          <td width="132"><strong>Name </strong></td>
                          <td width="116"><strong>Business Unit </strong></td>
                          <td width="307"><strong>Prize</strong></td>
                        </tr>
                        <tr valign="top">
                          <td>Karen MacQuarrie</td>
                          <td>TPW</td>
                          <td>$300 Gift Certificate from Future Shop</td>
                        </tr>
                        <tr valign="top">
                          <td>Shelly Hutt</td>
                          <td>BPIM</td>
                          <td>$100 Gift Certificate from Kent</td>
                        </tr>
                        <tr valign="top">
                          <td>Alan Taylor</td>
                          <td>TPW</td>
                          <td>$100 Gift Certificate from Home Depot</td>
                        </tr>
                        <tr valign="top">
                          <td>Samantha Charron</td>
                          <td>Comm Dev </td>
                          <td>$100 Gift Certificate from Home Sense</td>
                        </tr>
                      </table>
                      <p>&nbsp;</p>
                      <p>Thank you to the Project Sponsors, Geri Kaiser and Wayne Anstey, who generously provided the prizes.<br />
                      </p>
                      <p>&nbsp;                      </p>
                      <p>Please continue to send us your comments regarding the tutorial. Your feedback is both valued and appreciated.</p>
                      <table class="formtable" width="575" border="0" cellspacing="2" cellpadding="8" style="border:1px solid #dfe7ee;">
                                <tr>
                                  <td width="23%" valign="middle" bgcolor="#F2F4F9"><strong>Your Name</strong></td>
                                  <td width="77%" valign="middle" bgcolor="#F2F4F9"><label for="select"></label>
                                  <input class="main" type="text" name="Name" id="Name" /></td>
                                </tr>
                                <tr>
                                  <td valign="middle" bgcolor="#F2F4F9"><strong>Your Email</strong></td>
                                  <td valign="middle" bgcolor="#F2F4F9"><label for="select"></label>
                                      <input class="main" type="text" name="user_email" id="user_email" /></td>
                                </tr>
                                <tr>
                                  <td valign="middle" bgcolor="#F2F4F9"><strong>Tutorial</strong></td>
                                  <td valign="middle" bgcolor="#F2F4F9"><label for="textfield">
                                    <select name="Completed" class="main" id="subject">
                                      <option value="Tutorial 1" selected="selected">Tutorial 1 - Taking a Service Request</option>
                                      <option value="Tutorial 2">Tutorial 2 - Searching &amp; Viewing Information</option>
                                      <option value="Tutorial 3">Tutorial 3 - Actioning a Service Request Part 1</option>
                                      <option value="Tutorial 4">Tutorial 4 - Actioning a Service Request Part 2</option>                                    
                                    </select>
                                  </label></td>
                                </tr>
                                <tr>
                                  <td valign="middle" bgcolor="#F2F4F9"><strong>Comments&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
                                  <td valign="middle" bgcolor="#F2F4F9"><input type="text" size="3" name="sys_remLen" value="500" readonly="readonly" />
                                    Characters Remaining (Max: 500) </td>
                                </tr>
                                <tr>
                                  <td colspan="2" valign="top" bgcolor="#F2F4F9"><textarea name="Comments" rows="8" class="description" id="Comments" onkeydown="textCounter(this.form.Comments,this.form.sys_remLen,500);" onkeyup="textCounter(this.form.Comments,this.form.sys_remLen,500);"></textarea></td>
                                </tr>
                                <tr>
                                  <td height="29" colspan="3" valign="top" bgcolor="#F2F4F9"><label for="Submit"></label>
                                      <input type="reset" name="Reset" value="Reset" id="Reset" />
                                  <input type="submit" name="Submit" value="Submit" id="Submit" /></td>
                                </tr>
                      </table>
                    </form>
					</p>
				  <!-- InstanceEndEditable -->					</div>
				</div>
			
			</div>
		</div>
	</div>
	
	<div id="SiteFooterShadow">
		<div id="SiteFooterShadowRight">&nbsp;</div>
		<div id="SiteFooterShadowLeft">&nbsp;</div>
		<div class="ClearBoth"></div>
	</div>
	
	<div id="SiteFooter">
		<div id="SiteFooterText">
			<div id="SiteFooterLinks"><!--#include virtual="/System/_includes/SiteFooter.html"--></div>
			<div id="SiteFooterDateLastUpdated"><!--#config TIMEFMT = "This page was last updated: %A, %B %d, %Y at %I:%M %p"--><!--#echo var="LAST_MODIFIED"--></div>
		</div>
		<div id="SiteFooterHRMLogo"><a href="http://www.halifax.ca/" target="_blank"><img src="/System/_images/HRMLogo.jpg" alt="Goto: Halifax.ca" title="Goto: Halifax.ca" border="0" /></a></div>
		<div class="ClearBoth"></div>
	</div>
	
</div>


</body>
<!-- InstanceEnd --></html>
